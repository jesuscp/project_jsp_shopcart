<%@ page session="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Entidad.Carrito"%>
<%@ page import="Entidad.Productos"%>
<%@ page import="Controlador.controller"%>

<%
//declaramos variable tipo sesion
HttpSession sesion = request.getSession();
//obtenemos el id del producto a actualizar
String IdProducto = request.getParameter("id");
//obtenemos la cantidad a actualizar
int cantidad = Integer.parseInt(request.getParameter("cant"));
//declaramos una variable tipo arraylist de carrito
ArrayList<Carrito> objListC = new ArrayList<>();
//asignamos el valor de la sesi�n a la variable
objListC = (ArrayList)sesion.getAttribute("carrito");

//creamos una variable para que ayude a tener un control de los indices de la lista
int index = 0;
//se hace recorrido en for de la lista basandose en una varible tipo Carrito.	
for(Carrito c:objListC){
	//validamos si el id del recorrido es igual al que enviamos
	if(c.getIdProducto().equals(IdProducto)){
		//actualizamos la cantidad del elemento previo con la cantidad recibida
		c.setCantidad(cantidad);
		//actualizamos el elemento de la lista
		objListC.set(index, c);
		//termiamos el recorrido
		break;
	}
	//indice se aumenta de 1 en 1
	index++;
}
//no hacemos validaci�n de vac�o, ya que al actualizar significa que hay productos
//asignamos a la sesi�n la nueva lista
sesion.setAttribute("carrito", objListC);
//redireccionamos a carrito.jsp
response.sendRedirect("carrito.jsp");

%>
 