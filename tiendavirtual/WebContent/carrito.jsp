<%@ page session="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Entidad.Carrito"%>
<%@ page import="Entidad.Productos"%>
<%@ page import="Controlador.controller"%>
<%@ page import="utils.utilitarios"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="CSS/bootstrap.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link href="CSS/css.css" rel="stylesheet" type="text/css">
<link href="CSS/Footer.css" rel="stylesheet" type="text/css">
<link href="CSS/footerG.css" rel="stylesheet" type="text/css">
<title>Kaparazon</title>
</head>
<body>

<%
controller control = new controller();
//declaramos una variable de tipo HttpSession
HttpSession sesion = request.getSession();


ArrayList<Carrito> objListC = new ArrayList<>();
//validamos si se est�n enviando parametros a carrito.jsp
if (request.getParameter("id") != null && request.getParameter("txtcan") != null) {

	String IdProducto = request.getParameter("id");
	int Cantidad = Integer.parseInt(request.getParameter("txtcan"));
	double precio = Double.parseDouble(request.getParameter("txtPrecio"));
	//instanciamos la variable de Carrito con los valores enviados
	Carrito objC = new Carrito(IdProducto, Cantidad, precio);
	//validamos si la sesi�n es diferente de null
	if (sesion.getAttribute("carrito") != null) {
		//obtenemos la lista de sesi�n
		objListC = (ArrayList) sesion.getAttribute("carrito");
		int index = 0;
		boolean estado = false;
		//buscamos productos repetidos	
		for (Carrito c : objListC) {
			if (c.getIdProducto().equals(IdProducto)) {
				//si el producto existe se suma la cantidad a la actual
				c.setCantidad(c.getCantidad() + Cantidad);
				objListC.set(index, c);
				//actualizamos el estado a true para que no lo agregue a la lista ya que est� actualizando
				estado = true;
				break;
			}
			index++;
		}
		if (!estado){
			//de cumplirse se registra en la lista
			objListC.add(objC);
		}
	} else {
		objListC.add(objC);

	}

	sesion.setAttribute("carrito", objListC);

} else {
	//si no.. asignamos el valor de la sesi�n a la lista
	objListC = (ArrayList)sesion.getAttribute("carrito");
}

//variable que almacenar� la suma y la cantidad con un separador
String getCarro = "0-0";
String getUsuario = "<a href=login.jsp>Iniciar sesi�n</a>";

//instancia de utilitarios
	utilitarios objU = new utilitarios();

//preguntamos si el atributo de carrito es diferente de null
if (sesion.getAttribute("carrito") != null) {
	//cargamos variable con la informaci�n actual del carro
	getCarro = objU.CargarCarro(sesion);
}
if (sesion.getAttribute("login") != null) {
	getUsuario = objU.CargarUsuario(sesion);
}

%>

	<div class="main-container">

		<!-- 	BARRA DE NAVEGACION -->
		<nav id="navBar" class="navbar navbar-toggleable-md navbar-light">
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarKaparazon"
			aria-controls="navbarKaparazon" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!-- 		CARRITO DE COMPRAS CON DETALLE -->
			
		<div class="carritoContainer">
				<div class="relativa-carrito">
					<a href="carrito.jsp" class="sinSub fa fa-cart-plus fa-3x" aria-hidden="true"></a>
				</div>
				<div class="col-11 noPadding textoCarritoContainer">
				<!-- Usamos la variable haciendo split y mostrando la posici�n actual del peque�o arreglo -->
					<p class="relativa1">Tienes <%=getCarro.split("-")[1]%> producto(s)</p>
					<br>
					<p class="relativa2">
						Total: 
						US$ <%=getCarro.split("-")[0]%></p>
				</div>			
		</div>
		<div class="carritoContainer2">
		     <i class="fa fa-user-circle-o fa-2x" aria-hidden="true"></i>
		     <br>
		     <span><%= getUsuario %></span>
		</div>
	 
	
				<a class="navbar-brand active" href="Main.jsp"> <img
			src="Assets/Icon/favicon_32.ico"
			class="d-inline-block align-top navBarImg logo" alt="Icono">
			Kaparazon
		</a>
		<div class="collapse navbar-collapse" id="navbarKaparazon">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item"><a class="nav-link" href="index.jsp">Categorias</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Pedidos</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Nosotros</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Ayuda</a></li>
			</ul>
		</div>
	
		</nav>
	</div>

	<div class="container-shop">
			<h1>Carrito de Compras</h1>
			<hr>
			<% 
			double suma = 0;
		if(request.getParameter("buy")==null){
			if(objListC != null) {
				%>
			<table border="1" class="table table-bordered">
				<tr>
					<td>Item</td>
					<td>IdProducto</td>
					<td>Descripci�n</td>
					<td>Precio Unidad</td>
					<td>Imagen</td>
					<td>Cantidad</td>
					<td>Sub-total</td>
					<td>Opciones</td>
				</tr>
				<%
					int item = 1;
				
				
					
					for (Carrito o : objListC) {
						Productos pro = new Productos();
						pro = control.VerDetalleProductos(o.getIdProducto());
						String img = "<img src=img/" + pro.getFoto().split(",")[0] + "  width=50px height=50px>";
						String enlace = "<a class='trash fa fa-trash-o fa-2x' aria-hidden='true' title='Eliminar' onclick=eliminar('" + o.getIdProducto() + "')></a>";
						String act = "<a class='trash fa fa-pencil-square-o fa-2x' aria-hidden='true' title='Actualizar' onclick=actualizar('" + o.getIdProducto() + "'," + item + ")></a>";
						double subtotal = o.getCantidad() * pro.getPrecioUnidad();
						suma += subtotal;
				%>
				
				
				<tr>
					<td align="center"><%=item%></td>
					<td><%=o.getIdProducto()%></td>
					<td><%=pro.getDescripcion()%></td>
					<td align="right">US$ <%= pro.getPrecioUnidad() %></td>
					<td><%=img%></td>
					<td align="right"><input type="text" class="cant-prod" id="txtCant<%= item %>"
						value="<%=o.getCantidad()%>"></td>
					<td align="right">US$ <%= String.format("%.2f", subtotal) %></td>
					<td><%=enlace%><%=act%></td>
					<input type="hidden" id="txtStock<%= item %>" value="<%= pro.getStock() %>">
				</tr>
				<%
					item++;
					}
				%>
				<tr>
					<td colspan=7 align="right">TOTAL A PAGAR:</td>
					<td align="right">US$ <%= String.format("%.2f", suma) %></td>
				</tr>
			</table>
			<% if(sesion.getAttribute("login") == null){ %>
				<div class="alert alert-danger" role="alert">
  					<p class="alert-link">�Tienes que acceder a tu cuenta para realizar la compra!</p>
				</div>
			<% } %>
			<div style="clear:both;"></div>
			<div class="row MarginP">
				<a class="btn btn-warning" href="index.jsp">Seguir comprando</a>
				<a class="btn btn-warning" href="pagar.jsp?total=<%=suma%>">Pagar</a>
				<a class="btn btn-warning" href="suprimir.jsp?id=all">Limpiar carrito</a>
			</div>
			<% }else { %>
			<div class="alert alert-info" role="alert">
  				<p class="alert-link">No tienes productos en el carrito</p>
			</div>
		
					<div class="row MarginP">
						<a class="btn btn-warning" href="index.jsp">Seguir comprando</a>
					</div>
			<% }
		 }else{ %>
			
			 <div class="alert alert-success" role="alert">
  				<p class="alert-link">�Tu compra se realiz� correctamente!</p>
			</div>
			
		 <% }%>
			<div style="clear:both;"></div>	
		</div>

	<script type="text/javascript" src="JS/tether.min.js"></script>
	<script type="text/javascript" src="JS/jquery-3.2.1.js"></script>
	<script type="text/javascript" src="JS/bootstrap.js"></script>
	<script type="text/javascript" src="JS/JavaScript.js"></script>
	<script>
			//funci�n para actualizar cantidad
			function actualizar(id, indice) {
				//preguntamos si la cantidad ingresada es un numero
				if($.isNumeric($('#txtCant'+indice).val())){
					//se valida si lo ingresado es mayor al stock actual
					if(parseInt($('#txtCant'+indice).val()) > parseInt($('#txtStock'+indice).val())){
						alert('La cantidad no puede ser mayor al stock actual (' + $('#txtStock'+indice).val() + ').');
						//seteamos con valor m�ximo
						$('#txtCant'+indice).val($('#txtStock'+indice).val());
					}else{
						//se env�a hac�a actualizar.jsp
						location.href = "actualizar.jsp?id=" + id + "&cant=" + $('#txtCant'+indice).val();
					}
				}else{
					//seteamos el valor de la cantidad
					$('#txtCant'+indice).val(1);
					alert('Solo debe ingresar n�meros en la cantidad.');
				}
			}
			function eliminar(id){
				//validamos si en verdad se quiere eliminar el registro
				if (confirm("�Desea eliminar el producto: " + id + "?")) {
					//de ser as� se env�a el id a suprimir.jsp
					location.href = "suprimir.jsp?id=" + id;
				} 
			}
		</script>
</body>
<footer class="footerG">
 <div class="container">
	<p class="marginTextFooter">Kaparazon� Todos los Derechos Reservados</p>
 </div>
</footer>

</html>