<%@ page session="true"%>
<%@ page import="utils.utilitarios"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<link href="CSS/bootstrap.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link href="CSS/css.css" rel="stylesheet" type="text/css">
<link href="CSS/Footer.css" rel="stylesheet" type="text/css">
<title>Kaparazon</title>
</head>
<body>

	<%
		HttpSession sesion = request.getSession();
		String getCarro = "0-0";
		String getUsuario = "<a href=login.jsp>Iniciar sesi�n</a>";
		if (sesion.getAttribute("carrito") != null) {
			//instancia de utilitarios
			utilitarios objU = new utilitarios();
			//cargamos variable con la informaci�n actual del carro
			getCarro = objU.CargarCarro(sesion);
		}
		if (sesion.getAttribute("login") != null) {
			utilitarios objU = new utilitarios();
			getUsuario = objU.CargarUsuario(sesion);
		}
	%>
	<div class="main-container">

		<!-- 	BARRA DE NAVEGACION -->
		<nav id="navBar" class="navbar navbar-toggleable-md navbar-light">
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarKaparazon"
			aria-controls="navbarKaparazon" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!-- 		CARRITO DE COMPRAS CON DETALLE -->
		<div class="carritoContainer">
				<div class="relativa-carrito">
					<a href="carrito.jsp" class="sinSub fa fa-cart-plus fa-3x"
						aria-hidden="true"></a>
				</div>
				<div class="col-11 noPadding textoCarritoContainer">
					<p class="relativa1">
						Tienes
						<%=getCarro.split("-")[1]%>
						producto(s)
					</p>
					<br>
					<p class="relativa2">
						Total: 
						US$ <%=getCarro.split("-")[0]%></p>
				</div>
		</div>
		<div class="carritoContainer2">
		     <i class="fa fa-user-circle-o fa-2x" aria-hidden="true"></i>
		     <br>
		     <span><%= getUsuario %></span>
		</div>
		<a class="navbar-brand active" href="Main.jsp"> <img
			src="Assets/Icon/favicon_32.ico"
			class="d-inline-block align-top navBarImg logo" alt="Icono">
			Kaparazon
		</a>
		<div class="collapse navbar-collapse" id="navbarKaparazon">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item"><a class="nav-link" href="index.jsp">Categorias</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Pedidos</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Nosotros</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Ayuda</a></li>
			</ul>
		</div>
		</nav>

		<div class="container">
			<!-- 		BANNER CON IMAGENES -->
			<div class="row">
				<div class="col l12 bannerMargin">
					<div id="kaparazonBanner" class="carousel slide "
						data-ride="carousel">
						<div id="bannerK" class="carousel-inner" role="listbox"></div>
						<a class="carousel-control-prev" href="#kaparazonBanner"
							role="button" data-slide="prev"></a> <a
							class="carousel-control-next" href="#kaparazonBanner"
							role="button" data-slide="next"></a>
					</div>
				</div>
			</div>
			<!-- 		SECCIONES -->
			<div class="row">
				<div class="col-8">
					<div class="row">
						<img alt="Banner Kaparazon" src="Assets/Banner.png"
							class="bannerSeccion">
					</div>
					<p>Lorem Ipsum es simplemente el texto de relleno de las
						imprentas y archivos de texto. Lorem Ipsum ha sido el texto de
						relleno est�ndar de las industrias desde el a�o 1500, cuando un
						impresor (N. del T. persona que se dedica a la imprenta)
						desconocido us� una galer�a de textos y los mezcl� de tal manera
						que logr� hacer un libro de textos especimen. No s�lo sobrevivi�
						500 a�os, sino que tambien ingres� como texto de relleno en
						documentos electr�nicos, quedando esencialmente igual al original.
						Fue popularizado en los 60s con la creaci�n de las hojas
						"Letraset", las cuales contenian pasajes de Lorem Ipsum, y m�s
						recientemente con software de autoedici�n, como por ejemplo Aldus
						PageMaker, el cual incluye versiones de Lorem Ipsum.</p>
				</div>
				<div class="col-4">
					<h1>Ac� va un articulo</h1>
					<p>Lorem Ipsum es simplemente el texto de relleno de las
						imprentas y archivos de texto. Lorem Ipsum ha sido el texto de
						relleno est�ndar de las industrias desde el a�o 1500, cuando un
						impresor (N. del T. persona que se dedica a la imprenta)
						desconocido us� una galer�a de textos y los mezcl� de tal manera
						que logr� hacer un libro de textos especimen. No s�lo sobrevivi�
						500 a�os, sino que tambien ingres� como texto de relleno en
						documentos electr�nicos, quedando esencialmente igual al original.
						Fue popularizado en los 60s con la creaci�n de las hojas
						"Letraset", las cuales contenian pasajes de Lorem Ipsum, y m�s
						recientemente con software de autoedici�n, como por ejemplo Aldus
						PageMaker, el cual incluye versiones de Lorem Ipsum.</p>
				</div>
			</div>
			<!-- 			ACA VAN LOS BLOGS O ARTICULOS -->
			<div class="row">
				<div class="col-12 bannerMargin">

					<div id="titu" class="card-deck"></div>
				</div>

			</div>
		</div>

	</div>
	<div>
		<footer class="footer">
		<div class="contenedor">
			<div class="contacto">
				<img class="imgMarginTop" src="Assets/Logo.png"
					alt="logotipo blanco" /> <a class="paddingTelefono"
					href="tel:+573024456678"><strong>Telefono</strong> <span>3024456678</span></a>
				<a href="mailto:contacto@invie.com"><strong>E-mail</strong> <span>contacto@invie.com</span></a>
			</div>
			<form class="formulario">
				<div class="col1">
					<label for="nombre">Nombre</label> <input type="text" required
						id="nombre" name="nombre" /> <label for="email">E-mail</label> <input
						type="email" required id="email" name="email" />
					<div class="sexo">
						<label for="mujer"> <input type="radio" id="mujer" checked
							name="sexo" value="mujer"> mujer
						</label> <label for="hombre"> <input type="radio" id="hombre"
							name="sexo" value="hombre"> hombre
						</label>
					</div>
					<div class="intereses">
						<label for="cotizacion"> <input type="checkbox" checked
							id="cotizacion" name="intereses" value="cotizacion">
							Cotizaci�n
						</label> <label for="reclamos"> <input type="checkbox"
							id="reclamos" name="intereses" value="reclamos"> Reclamos
						</label> <label for="comentarios"> <input type="checkbox"
							id="comentarios" name="intereses" value="comantarios">
							Comentarios
						</label> <label for="otros"> <input type="checkbox" id="otros"
							name="intereses" value="otros"> Otros
						</label>
					</div>
				</div>
				<div class="col2">
					<label for="comentarios">Comentarios</label>
					<textarea name="comantarios" id="comentarios" cols="30" rows="7"></textarea>

					<div class="modalPrueba">
						<!-- Button trigger modal -->
						<button type="button" class="btn btnmodal" data-toggle="modal"
							data-target="#ModalForm">Enviar</button>

						<!-- Modal -->
						<div class="modal fade" id="ModalForm" tabindex="-1" role="dialog"
							aria-labelledby="ModalTitulo" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="ModalTitulo">Kaparazon</h5>
										<button type="button" class="close" data-dismiss="modal"
											aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">Nos estaremos comunicando contigo</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary"
											data-dismiss="modal">Cerrar</button>
									</div>
								</div>
							</div>
						</div>
					</div>


					<!-- 					<input type="submit" value="Enviar" class="button" /> -->
				</div>
			</form>
		</div>
		</footer>
	</div>



	<script type="text/javascript" src="JS/tether.min.js"></script>
	<script type="text/javascript" src="JS/jquery-3.2.1.js"></script>
	<script type="text/javascript" src="JS/bootstrap.js"></script>
	<script type="text/javascript" src="JS/JavaScript.js"></script>
</body>
</html>

















