--------------------------------
-- Script de Complemento
-- Base de datos : tienda
-- Fecha : 23-Junio-2017
--------------------------------
use tienda;

-- Tabla : Categorias
drop table if exists Categorias;
create table Categorias(
	IdCategoria CHAR(6) NOT NULL,
    Descripcion VARCHAR(50) NOT NULL,
    Foto VARCHAR(50) NOT NULL,
    Estado CHAR(1) NOT NULL,
	check(Estado IN ('0','1')),
    primary key(IdCategoria)
);
insert Categorias values('cat001','Televisor','cat001.jpg','1'),
('cat002','Lavadora','cat002.jpg','1'),
('cat003','Refrigeradora','cat003.jpg','1'),
('cat004','Equipo de Sonido','cat004.jpg','1'),
('cat005','Reproductor BlueRay','cat005.jpg','1'),
('cat006','Cocina a Gas','cat006.jpg','1');

select * from Categorias;

-- Tabla : Productos
drop table if exists Productos;
create table Productos(
	IdProducto CHAR(6) NOT NULL,
    IdCategoria VARCHAR(6) NOT NULL,
    Descripcion VARCHAR(50) NOT NULL,
    PrecioUnidad DECIMAL NOT NULL,
    Stock INT NOT NULL,
    Foto VARCHAR(50) NOT NULL,
    Estado CHAR(1) NOT NULL,
    CHECK(PrecioUnidad > 0),
    CHECK(Stock > 0),
    CHECK(Estado IN ('0','1')),
    PRIMARY KEY(IdProducto),
    FOREIGN KEY(IdCategoria) REFERENCES Categorias(IdCategoria)
);

insert Productos values('pro001','cat001','Televisor LG',1500,10,'pro001.jpg','1'),
('pro002','cat001','Televisor SAMSUNG',1600,10,'pro002.jpg','1'),
('pro003','cat001','Televisor SONY',1800,10,'pro003.jpg','1'),
('pro004','cat001','Televisor MIRAY',1100,10,'pro004.jpg','1'),
('pro005','cat002','Lavadora LG',2200,10,'pro005.jpg','1'),
('pro006','cat002','Lavadora G&E',2500,10,'pro006.jpg','1'),
('pro007','cat002','Lavadora Coldex',2000,10,'pro007.jpg','1'),
('pro008','cat002','Lavadora Faeda',1800,10,'pro008.jpg','1'),
('pro009','cat003','Refrigeradora LG',1900,10,'pro008.jpg','1'),
('pro010','cat003','Refrigeradora G&E',2200,10,'pro010.jpg','1'),
('pro011','cat003','Refrigeradora Coldex',1200,10,'pro011.jpg','1'),
('pro012','cat003','Refrigeradora Faeda',1100,10,'pro012.jpg','1'),
('pro013','cat004','Equipo de Sonido LG',1400,10,'pro013.jpg','1'),
('pro014','cat004','Equipo de Sonido Sony',1600,10,'pro014.jpg','1'),
('pro015','cat004','Equipo de Sonido Samsung',1500,10,'pro015.jpg','1'),
('pro016','cat004','Equipo de Sonido Pionner',1900,10,'pro016.jpg','1');

select * from Productos;
