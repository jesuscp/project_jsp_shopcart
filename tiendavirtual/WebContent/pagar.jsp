<%@ page session="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Entidad.*"%>
<%@ page import="Controlador.controller"%>
<%@ page import="utils.utilitarios"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%
HttpSession sesion = request.getSession();
controller c = new controller();

if(sesion.getAttribute("login")!=null){
	ArrayList<Carrito> objListC = new ArrayList<>();
	objListC = (ArrayList)sesion.getAttribute("carrito");
	
	Cliente objCli = new Cliente();
	objCli = (Cliente)sesion.getAttribute("login");
	
	double monto = Double.parseDouble(request.getParameter("total"));
	
	String r1 = c.RegistrarCabe(objCli.getDni(), monto);
	String r2 = "";
	if(r1.equals("ok")){
		r2 = c.RegistrarDeta(objListC);
	}
	sesion.setAttribute("carrito", null);
	response.sendRedirect("carrito.jsp?buy=success");
	
	
}else{
	response.sendRedirect("registro.jsp");
}

%>

</body>
</html>