
<%@ page import="Controlador.*"%>
<%@ page import="Entidad.*"%>
<%@ page session="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="utils.utilitarios"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="CSS/bootstrap.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link href="CSS/css.css" rel="stylesheet" type="text/css">
<link href="CSS/Footer.css" rel="stylesheet" type="text/css">
<link href="CSS/footerG.css" rel="stylesheet" type="text/css">
<title>Kaparazon</title>
</head>
<body>
 
	<%
		controller ObjC = new controller();
		//declaramos una variable de tipo HttpSession
		HttpSession sesion = request.getSession();
		//variable que almacenar� la suma y la cantidad con un separador
		String getCarro = "0-0";
		String getUsuario = "<a href=login.jsp>Iniciar sesi�n</a>";
		//preguntamos si el atributo de carrito es diferente de null
		if (sesion.getAttribute("carrito") != null) {
			//instancia de utilitarios
			utilitarios objU = new utilitarios();
			//cargamos variable con la informaci�n actual del carro
			getCarro = objU.CargarCarro(sesion);
		}
		
		if (sesion.getAttribute("login") != null) {
			utilitarios objU = new utilitarios();
			getUsuario = objU.CargarUsuario(sesion);
		}
		
	%>
	<div class="main-container">

		<!-- 	BARRA DE NAVEGACION -->
		<nav id="navBar" class="navbar navbar-toggleable-md navbar-light">
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarKaparazon"
			aria-controls="navbarKaparazon" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!-- 		CARRITO DE COMPRAS CON DETALLE -->
		<div class="carritoContainer">
				<div class="relativa-carrito">
					<a href="carrito.jsp" class="sinSub fa fa-cart-plus fa-3x"
						aria-hidden="true"></a>
				</div>
				<div class="col-11 noPadding textoCarritoContainer">
					<!-- Usamos la variable haciendo split y mostrando la posici�n actual del peque�o arreglo -->
					<p class="relativa1">
						Tienes
						<%=getCarro.split("-")[1]%>
						producto(s)
					</p>
					<br>
					<p class="relativa2">
						Total: 
						US$ <%=getCarro.split("-")[0]%></p>
				</div>
		</div>
		<div class="carritoContainer2">
		     <i class="fa fa-user-circle-o fa-2x" aria-hidden="true"></i>
		     <br>
		     <span><%= getUsuario %></span>
		</div>
		<a class="navbar-brand active" href="Main.jsp"> <img
			src="Assets/Icon/favicon_32.ico"
			class="d-inline-block align-top navBarImg logo" alt="Icono">
			Kaparazon
		</a>
		<div class="collapse navbar-collapse" id="navbarKaparazon">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item"><a class="nav-link" href="index.jsp">Categorias</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Pedidos</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Nosotros</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Ayuda</a></li>
			</ul>
		</div>
		</nav>
	</div>

	<%
		ArrayList<Categorias> ListaC = new ArrayList<>();
		ListaC = ObjC.ListarCategorias();
		int col = 0;
	%>
	 
		<div class="container contenedor-arr">

			<div class="bannerMargin">
				<h1>Nuestras Categor�as</h1>
			</div>
			<hr>
			<div class="row">
				<%
				
					for (Categorias Obj : ListaC) {
						String foto = "<img src=img/" + Obj.getFoto() + " width=300 height=250>";
						String enlace = "verproductos.jsp?id=" + Obj.getIdCategoria() + "&ncatg=" + Obj.getDescripcion().replace(" " ,"*");
				%>		
							 <div class='col-sm-6 col-md-4'>
								<div class='thumbnail'>
									<a class='sinSub' href=<%= enlace.replace("*", "-") %>>
										<%= foto %>
									</a>
									<div class='caption'>
										<a class='sinSub' href=<%= enlace.replace("*", "-") %>>
											<h4 class='card-text'><%= Obj.getDescripcion() %></h4>
										</a>
									</div>
								</div>
							</div>
				<% }%>
			</div>
		</div>


	<script type="text/javascript" src="JS/tether.min.js"></script>
	<script type="text/javascript" src="JS/jquery-3.2.1.js"></script>
	<script type="text/javascript" src="JS/bootstrap.js"></script>
	<script type="text/javascript" src="JS/JavaScript.js"></script>
</body>
<footer class="footerG">
 <div class="container">
	<p class="marginTextFooter">Kaparazon� Todos los Derechos Reservados</p>
 </div>
</footer>
</html>





















