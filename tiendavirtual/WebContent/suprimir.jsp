<%@ page session="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="Entidad.Carrito"%>

<%
//declaramos variable tipo sesion
HttpSession sesion = request.getSession();
//obtenemos el id a suprimir
String IdProducto = request.getParameter("id");


//declaramos una variable tipo arraylist de carrito
ArrayList<Carrito> objListC = new ArrayList<>();
//asignamos el valor de la sesi�n a la variable
objListC = (ArrayList)sesion.getAttribute("carrito");

if(!IdProducto.equalsIgnoreCase("all")){
	out.print("aqui no debe");
//creamos una variable para que ayude a tener un control de los indices de la lista
int index = 0;
//se hace recorrido en for de la lista basandose en una varible tipo Carrito.	
for(Carrito c:objListC){
	//validamos si el id del recorrido es igual al que enviamos
	if(c.getIdProducto().equals(IdProducto)){
		//eliminamos si se encuentra de acuerdo al indice
		objListC.remove(index);
		//termiamos el recorrido
		break;
	}
	//indice se aumenta de 1 en 1
	index++;
}

} else {
	
	objListC = new ArrayList<>();
}
 //validamos si la lista est� vacia
if(objListC.isEmpty()){
	//dejamos como null el carrito ya que el valor null es diferente a vacio
	sesion.setAttribute("carrito", null);
}else{
	//si tiene info, la volvemos a cargar en la sesi�n
	sesion.setAttribute("carrito", objListC);
}
//redireccionamos a carrito.jsp
response.sendRedirect("carrito.jsp");


%>