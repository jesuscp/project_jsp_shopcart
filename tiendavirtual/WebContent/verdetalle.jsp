<%@page import="java.sql.Array"%>
<%@ page import="Controlador.*"%>
<%@ page import="Entidad.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ page session="true"%>
<%@ page import="utils.utilitarios" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="CSS/bootstrap.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link href="CSS/css.css" rel="stylesheet" type="text/css">
<link href="CSS/Footer.css" rel="stylesheet" type="text/css">
<link href="CSS/footerG.css" rel="stylesheet" type="text/css">
<title>Kaparazon</title>
</head>
<body>

<%
controller ObjC = new controller();
//declaramos una variable de tipo HttpSession
HttpSession sesion = request.getSession();
//variable que almacenar� la suma y la cantidad con un separador
String getCarro = "0-0";
String getUsuario = "<a href=login.jsp>Iniciar sesi�n</a>";

//preguntamos si el atributo de carrito es diferente de null
if (sesion.getAttribute("carrito") != null) {
	//instancia de utilitarios
	utilitarios objU = new utilitarios();
	//cargamos variable con la informaci�n actual del carro
	getCarro = objU.CargarCarro(sesion);
}

if (sesion.getAttribute("login") != null) {
	utilitarios objU = new utilitarios();
	getUsuario = objU.CargarUsuario(sesion);
}
%>

	<div class="main-container">

		<!-- 	BARRA DE NAVEGACION -->
		<nav id="navBar" class="navbar navbar-toggleable-md navbar-light">
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarKaparazon"
			aria-controls="navbarKaparazon" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<!-- 		CARRITO DE COMPRAS CON DETALLE -->
		<div class="carritoContainer">			
				<div class="relativa-carrito">
					<a href="carrito.jsp" class="sinSub fa fa-cart-plus fa-3x" aria-hidden="true"></a>
				</div>
				<div class="col-11 noPadding textoCarritoContainer">
				<!-- Usamos la variable haciendo split y mostrando la posici�n actual del peque�o arreglo -->
					<p class="relativa1">Tienes <%=getCarro.split("-")[1]%> producto(s)</p>
					<br>
					<p class="relativa2">
						Total: 
						US$ <%=getCarro.split("-")[0]%></p>
				</div>
		</div>
		<div class="carritoContainer2">
		     <i class="fa fa-user-circle-o fa-2x" aria-hidden="true"></i>
		     <br>
		     <span><%= getUsuario %></span>
		</div>
				<a class="navbar-brand active" href="Main.jsp"> <img
			src="Assets/Icon/favicon_32.ico"
			class="d-inline-block align-top navBarImg logo" alt="Icono">
			Kaparazon
		</a>
		<div class="collapse navbar-collapse" id="navbarKaparazon">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item"><a class="nav-link" href="index.jsp">Categorias</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Pedidos</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Nosotros</a>
				</li>
				<li class="nav-item"><a class="nav-link" href="#">Ayuda</a></li>
			</ul>
		</div>
		</nav>
</div>



<form action="carrito.jsp" onsubmit="return validaStock(this);">
<%
	String IdProd = request.getParameter("id");
	Productos ObjP = new Productos();
	ObjP = ObjC.VerDetalleProductos(IdProd);
	int col = 0;
%>
<input type="hidden" name="id" value="<%= ObjP.getIdProducto() %>">
<%  
if (ObjP != null){
String[] fotos = {};
fotos = ObjP.getFoto().split(",");
String foto = "<img id='vistaPrevia' src=img/"+ fotos[0] +" width=400px height=600px>";
%>

<div class="container">
	<br/>
	<h4>Inicio > <%= request.getParameter("ncatg").replace("-", " ") %></h4>
	<hr>

	<div class="row">

		<div class="col-sm-2">
		    <section class="panel">
		        <div class="panel-body imgPrin">
				 <% for(int i = 0; i < fotos.length; i++){ %>
					<img  src="img/<%= fotos[i] %>" onmouseover="ver(this.src)" class="imgThumb">
				<% } %>
		        </div>
		        
		    </section>
		</div>

		<div class="col-sm-5">

		    <section class="panel">
		        <div class="panel-body">
					<%=foto%>
		        </div>
		    </section>
		</div>

		<div class="col-sm-4">
		    <section class="panel">
		        <div class="panel-body">
					<h4><%= ObjP.getDescripcion() %></h4>

					<p>
						<%= ObjP.getDesc_corta() %>
					</p>
					<div class="saleform_product_form">
						<p>
							<h5>Precio: <span class="price-prod">US$ <%=ObjP.getPrecioUnidad()%></span></h5>
							<input type="hidden" name="txtPrecio" value="<%=ObjP.getPrecioUnidad()%>"> 
						</p>
						<p>
							<h5>Stock: <small><%= ObjP.getStock() %></small></h5>
						</p>
						<p>
							<h5>Cantidad: <input class="cant-prod" name="txtcan" id="txtCantidad"></h5>						
						</p>					
						<input class="btn btn-warning btn-lg btn-shop" type="submit"  value="A�adir al carrito" >
						<input type="hidden" id="idStock" value="<%= ObjP.getStock() %>">
					</div>
		        </div>
		    </section>
		</div>
	</div>
	<div class="row MarginP">
		<h5>Descripci�n:</h5>
		<p>
			<%= ObjP.getDesc_larga() %>
		</p>
		<a class="btn btn-warning" href="javascript:history.back()">Seleccionar Otro producto</a>	  
	</div>
	<div style="clear:both;"></div>
</div>
<%
}
%>
	

</form>

<script>
function validaStock(){
	//se realiza una validaci�n similar que en carrito.jsp
	if($.isNumeric($('#txtCantidad').val())){
		if(parseInt($('#txtCantidad').val()) > parseInt($('#idStock').val())){
			alert('La cantidad no puede ser mayor al stock actual('+ $('#idStock').val() +').');
			$('#txtCantidad').val($('#idStock').val());
			//retornamos false ya que est� en onSubmit en el formulario y as� no haga petici�n
			return false;
		}
	}else{
		$('#txtCantidad').val(1);
		alert('Solo debe ingresar n�meros en la cantidad.');
		return false;
	}
}

function ver(src){
	$("#vistaPrevia").attr("src",src);
}

</script>


	<script type="text/javascript" src="JS/tether.min.js"></script>
	<script type="text/javascript" src="JS/jquery-3.2.1.js"></script>
	<script type="text/javascript" src="JS/bootstrap.js"></script>
	<script type="text/javascript" src="JS/JavaScript.js"></script>
</body>
<footer class="footerG">
<div class="contenedor">
<p class="marginTextFooter">Kaparazon� Todos los Derechos Reservados</p>
</div>
</footer>

</html>
