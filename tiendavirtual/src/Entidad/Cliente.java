package Entidad;

public class Cliente {
 
	private String dni;
	private String apellidos;
	private String nombres;
	private String correo;
	
	public Cliente(){
		
	}
	
	public Cliente(String dni, String apellidos, String nombres, String correo) {
		this.dni = dni;
		this.apellidos = apellidos;
		this.nombres = nombres;
		this.correo = correo;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	
}
