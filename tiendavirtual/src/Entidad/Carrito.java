package Entidad;

public class Carrito {
	private String IdProducto;
	private int Cantidad;
	private double Precio;
	
	public Carrito(){}
	
	public Carrito(String idProducto, int cantidad, double precio) {
		IdProducto = idProducto;
		Cantidad = cantidad;
		Precio = precio;
	}
	public String getIdProducto() {
		return IdProducto;
	}
	public void setIdProducto(String idProducto) {
		IdProducto = idProducto;
	}
	public int getCantidad() {
		return Cantidad;
	}
	public void setCantidad(int cantidad) {
		Cantidad = cantidad;
	}
	public double getPrecio() {
		return Precio;
	}
	public void setPrecio(double precio) {
		Precio = precio;
	}
	
	
	 
	
}
