package Entidad;

public class Categorias {
	// Campos o atributos
	private String IdCategoria;
	private String Descripcion;
	private String Foto;
	private char Estado;
	
	// M�todos Constructores
	public Categorias(){}
	
	public Categorias(String idCategoria, String descripcion, String foto, char estado) {
		IdCategoria = idCategoria;
		Descripcion = descripcion;
		Foto = foto;
		Estado = estado;
	}
	
	// Propiedades de Lectura y Escritura
	public String getIdCategoria() {
		return IdCategoria;
	}
	public void setIdCategoria(String idCategoria) {
		IdCategoria = idCategoria;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public String getFoto() {
		return Foto;
	}
	public void setFoto(String foto) {
		Foto = foto;
	}
	public char getEstado() {
		return Estado;
	}
	public void setEstado(char estado) {
		Estado = estado;
	}
	
	
	
}
