package Entidad;

public class Productos {
	// Campos o atributos
	private String IdProducto;
	private String IdCategoria;
	private String Descripcion;
	private double PrecioUnidad;
	private int Stock;
	private String Foto;
	private String desc_corta;
	private String desc_larga;
	
	public Productos(){}
	
	public Productos(String idProducto, String idCategoria, String descripcion, double precioUnidad, int stock,
			String foto, String desc_corta, String desc_larga, char estado) {
		 
		IdProducto = idProducto;
		IdCategoria = idCategoria;
		Descripcion = descripcion;
		PrecioUnidad = precioUnidad;
		Stock = stock;
		Foto = foto;
		this.desc_corta = desc_corta;
		this.desc_larga = desc_larga;
		Estado = estado;
	}
	public String getIdProducto() {
		return IdProducto;
	}
	public void setIdProducto(String idProducto) {
		IdProducto = idProducto;
	}
	public String getIdCategoria() {
		return IdCategoria;
	}
	public void setIdCategoria(String idCategoria) {
		IdCategoria = idCategoria;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public double getPrecioUnidad() {
		return PrecioUnidad;
	}
	public void setPrecioUnidad(double precioUnidad) {
		PrecioUnidad = precioUnidad;
	}
	public int getStock() {
		return Stock;
	}
	public void setStock(int stock) {
		Stock = stock;
	}
	public String getFoto() {
		return Foto;
	}
	public void setFoto(String foto) {
		Foto = foto;
	}
	public String getDesc_corta() {
		return desc_corta;
	}
	public void setDesc_corta(String desc_corta) {
		this.desc_corta = desc_corta;
	}
	public String getDesc_larga() {
		return desc_larga;
	}
	public void setDesc_larga(String desc_larga) {
		this.desc_larga = desc_larga;
	}
	public char getEstado() {
		return Estado;
	}
	public void setEstado(char estado) {
		Estado = estado;
	}
	private char Estado;
	
	// M�todos Constructores
	 
	
	
}
