package Modelo;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import Entidad.*;

public class model {
	// Campos o atributos
	private String Driver = "com.mysql.jdbc.Driver";
	private String URL = "jdbc:mysql://localhost:3306/DB_TIENDA_VIRTUAL";
	private String User = "root";
	private String Clave = "";
	private Connection Cn;
	private Statement Cmd;
	private CallableStatement Stm;
	private ResultSet Rs;
	private ArrayList<Categorias> ListaC;
	private ArrayList<Productos> ListaP;
	public String nroFatura ="";
	// M�todo Constructor
	public model(){
		try {
			Class.forName(Driver);
			Cn = DriverManager.getConnection(URL,User,Clave);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	// M�todo para devolver las categorias
	public ArrayList<Categorias> ListarCategorias(){
		ListaC = new ArrayList<>();
		String SQL = "SELECT * FROM Categorias";
		try {
			Cmd = Cn.createStatement();
			Rs = Cmd.executeQuery(SQL);
			while(Rs.next()){
				Categorias ObjC = new Categorias(
							Rs.getString("IdCategoria"),
							Rs.getString("Descripcion"),
							Rs.getString("Foto"),
							Rs.getString("Estado").charAt(0));
				ListaC.add(ObjC);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return ListaC;
	}
	
	// M�todo para devolver productos de una categoria
	public ArrayList<Productos> ListarProductos(String IdCat){
		ListaP = new ArrayList<>();
		String SQL = "SELECT * FROM Productos WHERE IdCategoria='"+IdCat+"'";
		try {
			Cmd = Cn.createStatement();
			Rs = Cmd.executeQuery(SQL);
			while(Rs.next()){
				Productos ObjP = new Productos(
							Rs.getString("IdProducto"),
							Rs.getString("IdCategoria"),
							Rs.getString("Nombre"),
							Rs.getDouble("PrecioUnidad"),
							Rs.getInt("Stock"),
							Rs.getString("Foto"),
							Rs.getString("Descripcion_corta"),
							Rs.getString("Descripcion_larga"),
							Rs.getString("Estado").charAt(0)
						);
				ListaP.add(ObjP);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return ListaP;
	}
	public Productos VerDetalleProducto(String id){
		Productos objP = null;
		String SQL = "select * from productos where IdProducto='" + id + "'";
		try {
			Cmd = Cn.createStatement();
			Rs = Cmd.executeQuery(SQL);
			if(Rs.next()){
				objP = new Productos(
						Rs.getString("IdProducto"),
						Rs.getString("IdCategoria"),
						Rs.getString("Nombre"),
						Rs.getDouble("PrecioUnidad"),
						Rs.getInt("Stock"),
						Rs.getString("Foto"),
						Rs.getString("Descripcion_corta"),
						Rs.getString("Descripcion_larga"),
						Rs.getString("Estado").charAt(0)
					);
			}
		} catch (Exception e) {
			System.out.println("Error => " + e.getMessage());
		}
		return objP;
	}
	public String RegistrarCliente(String dni, String ape, String nom, String correo, String pass){
		String _return = "";
		String sql = "insert into cliente values('" + dni + "','" + ape + "','" + nom + "','" + correo + "','" + pass + "')";
		System.out.println(sql);
	    try {
	    	Stm = Cn.prepareCall(sql);
			Stm.executeUpdate();
			
			_return = "Registrado";
		} catch (Exception e) {
			System.out.println("Error => " + e.getMessage());
		}
	    return _return;
	}
	
	public Cliente ValidaCliente(String dni, String pass){
		Cliente objCli = null;
		String sql = "select dni, apellidos, nombres, correo from cliente where dni='" + dni + "' and contrasena='" + pass + "'";
		try {
			Cmd = Cn.createStatement();
			Rs = Cmd.executeQuery(sql);
			if(Rs.next()){
				objCli = new Cliente(
						Rs.getString("dni"),
						Rs.getString("apellidos"),
						Rs.getString("nombres"),
						Rs.getString("correo")						
						);
			}
			
		} catch (Exception e) {
			System.out.println("Error => " + e.getMessage());
		}
		
		return objCli;
	}
	private String getCodigoFactura(){
	 
		String sql = "select ifnull(max(IdFactura),0) + 1 as ultimo from FACCAB";
		try {
			Cmd = Cn.createStatement();
			Rs = Cmd.executeQuery(sql);
			if(Rs.next()){
				nroFatura = Rs.getString("ultimo");
			}
		} catch (Exception e) {
			System.out.println("Error => " + e.getMessage());
		}
		return nroFatura;
	}
	
	public String RegistrarCabe(String cliente, double monto){
		nroFatura = getCodigoFactura();
		String _return ="";
		String sql = "insert into FACCAB values(" + nroFatura + ",'" + cliente + "',now(),"+ monto+")";
		try {
			Stm = Cn.prepareCall(sql);
			Stm.executeUpdate();
			_return = "ok";
		} catch (Exception e) {
			System.out.println("Error => " + e.getMessage());
		}
		
		return _return;
	}
	public String RegistrarDeta(ArrayList<Carrito> objP){
		String sql = "";
		String _return ="";
		
		try {
			for (Carrito c : objP) {
				sql = "insert into FACDET values(" + nroFatura +", '" + c.getIdProducto() + "', "+ c.getCantidad() +", "+ c.getPrecio() +")";
				Stm = Cn.prepareCall(sql);
				Stm.executeUpdate();
			}
			_return = "ok";
		} catch (Exception e) {
			System.out.println("Error => " + e.getMessage());
		}
		
		return _return; 
	}
}
