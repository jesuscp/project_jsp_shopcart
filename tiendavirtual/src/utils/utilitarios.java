package utils;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

import Controlador.controller;
import Entidad.Carrito;
import Entidad.Productos;
import Entidad.Cliente;

public class utilitarios {

	private controller ObjC = new controller();
	private ArrayList<Carrito> objListC = new ArrayList<>();
	private Cliente objCli = new Cliente();
	
	
	public String CargarCarro(HttpSession sesion){
		String _return = "";
		int cantidad = 0;
		double suma = 0;

		objListC = (ArrayList)sesion.getAttribute("carrito");

		for(Carrito o:objListC){
			Productos pro = new Productos();
			pro = ObjC.VerDetalleProductos(o.getIdProducto());
			cantidad += o.getCantidad();
			suma += o.getCantidad() * pro.getPrecioUnidad();
		}
		_return = String.format("%.2f", suma) + "-" + cantidad;
		return _return;
	}
	public String CargarUsuario(HttpSession sesion){
		String _return ="";
		objCli = (Cliente)sesion.getAttribute("login");
		
		_return = objCli.getNombres() + ", " + objCli.getApellidos() + " | <a href=cerrarsesion.jsp>X</a>";
		
		return _return;
	}
	
}
