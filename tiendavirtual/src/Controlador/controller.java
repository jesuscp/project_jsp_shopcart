package Controlador;
import Entidad.*;
import Modelo.*;
import java.util.ArrayList;

public class controller {
	// Campos o atributos
	private model ObjM;
	
	// M�todo Constructor
	public controller(){
		ObjM = new model();
	}
	
	// M�todos a exponer
	public ArrayList<Categorias> ListarCategorias(){
		return ObjM.ListarCategorias();
	}
	
	public ArrayList<Productos> ListarProductos(String IdCat){
		return ObjM.ListarProductos(IdCat);
	}
	
	public Productos VerDetalleProductos(String id){
		return ObjM.VerDetalleProducto(id);
	}
	
	public String RegistrarCliente(String dni, String ape, String nom, String correo, String pass){
		return ObjM.RegistrarCliente(dni, ape, nom, correo, pass);
	}
	public Cliente ValidaCliente(String dni, String pass){
		return ObjM.ValidaCliente(dni, pass);
	}
	public String RegistrarCabe(String cliente, double monto){
		return ObjM.RegistrarCabe(cliente, monto);
	}
	public String RegistrarDeta(ArrayList<Carrito> objP){
		return ObjM.RegistrarDeta(objP);
	}
}